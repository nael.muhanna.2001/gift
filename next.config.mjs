/** @type {import('next').NextConfig} */
const nextConfig = {
  compiler: {
    styledComponents: true,
  },
  env: {
    URL: "http://localhost:8000/api",
  },
};

export default nextConfig;
