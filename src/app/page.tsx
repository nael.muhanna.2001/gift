import CategoryList from "@/components/home/Card/Category/CategoryList";
import PointList from "@/components/home/Card/Point/PointList";

export default function Home() {
  return (
    <>
      <h1>home</h1>
      <CategoryList />
      <PointList />
    </>
  );
}
