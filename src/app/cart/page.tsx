import ProductCart from "@/components/cart/ProductCart";

const Cart = () => {
  const total = 500;
  return (
    <>
      <div className="min-h-screen bg-gray-100 pt-20">
        <h1 className="mb-10 text-center text-2xl font-bold">سلتي</h1>
        <div className="mx-auto  px-6 lg:flex lg:justify-around items-center">
          <div className=" w-2/3 m-2">
            <ProductCart />
            <ProductCart />
            <ProductCart />
            <ProductCart />
          </div>
          <div className="mt-6 h-full rounded-lg border bg-white p-6 shadow-md w-full lg:w-1/3 ">
            <div className="flex justify-between">
              <p className="text-lg font-bold">المجموع</p>
              <div className="">
                <p className="mb-1 text-lg font-bold">
                  {total}
                  <span className=" m-2 text-md">نقطة</span>
                </p>
              </div>
            </div>
            <button className="mt-6 w-full rounded-md bg-blue-500 py-1.5 font-medium text-blue-50 hover:bg-blue-600">
              Check out
            </button>
          </div>
        </div>
      </div>
    </>
  );
};

export default Cart;
