import LoginForm from "@/components/form/LoginForm";
import Image from "next/image";

const Login = () => {
  return (
    <>
      <div className="flex min-h-full flex-col justify-center py-6 sm:px-6 lg:px-8">
        <div className="sm:mx-auto sm:w-full sm:max-w-md">
          <Image
            className="mx-auto h-40 w-auto lg:h-56"
            width={500}
            height={500}
            src="/gift-logo.jpg"
            alt="Your Company"
          />
        </div>

        <div className=" sm:mx-auto sm:w-full sm:max-w-md">
          <div className="bg-white py-8 px-4 shadow sm:rounded-lg sm:px-10">
            <LoginForm />
          </div>
        </div>
      </div>
    </>
  );
};

export default Login;
