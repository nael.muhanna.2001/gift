"use client";
import Image from "next/image";
import Link from "next/link";
import Badge, { BadgeProps } from "@mui/material/Badge";
import { styled } from "@mui/material/styles";
import IconButton from "@mui/material/IconButton";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";
import AccountCircleIcon from "@mui/icons-material/AccountCircle";
import DashboardIcon from "@mui/icons-material/Dashboard";
const StyledBadge = styled(Badge)<BadgeProps>(({ theme }) => ({
  "& .MuiBadge-badge": {
    right: 3,
    top: 13,
    border: `2px solid #fb923c`,
    padding: "0 4px",
    background: "#fb923c",
  },
}));
const Header = () => {
  return (
    <>
      <header>
        <div className="w-full mx-auto h-22 flex justify-between items-center">
          <div className=" pr-2 mt-2">
            <Link href={"/cart"} className="fab">
              <IconButton aria-label="cart">
                <StyledBadge badgeContent={4} color="secondary">
                  <ShoppingCartIcon sx={{ fontSize: 30 }} />
                </StyledBadge>
              </IconButton>
            </Link>
            <Link href={"/login"} className="fab">
              <IconButton aria-label="user">
                <StyledBadge color="secondary">
                  <AccountCircleIcon sx={{ fontSize: 30 }} />
                </StyledBadge>
              </IconButton>
            </Link>
            <Link href={"/dashboard"} className="fab">
              <IconButton aria-label="dashboard">
                <StyledBadge color="secondary">
                  <DashboardIcon sx={{ fontSize: 30 }} />
                </StyledBadge>
              </IconButton>
            </Link>
          </div>
          <Link href={"/"}>
            <Image
              src="/ali-mosque-logo.svg"
              width={100}
              height={100}
              alt="Picture of the author"
            />
          </Link>
        </div>
      </header>
    </>
  );
};

export default Header;
