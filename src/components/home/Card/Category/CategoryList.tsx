import React from "react";
import CategoryCard from "./CategoryCard";
const category = [
  {
    key: "1",
    name: "جوائز المتميزين",
    image: "/category/bike.webp",
  },
  {
    key: "2",
    name: "جوائز متنوعة",
    image: "/category/chield-game.jpg",
  },
  {
    key: "3",
    name: "قرطاسية",
    image: "/category/school-thing.jpeg",
  },
];
const CategoryList = () => {
  return (
    <>
      <div className=" container mx-auto grid lg:grid-cols-2 2xl:grid-cols-3">
        {category.map((e) => {
          return <CategoryCard key={e.key} image={e.image} name={e.name} />;
        })}
      </div>
    </>
  );
};

export default CategoryList;
