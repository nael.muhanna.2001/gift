import Image from "next/image";
import Link from "next/link";
import { Category } from "@/utils/type";
const CategoryCard = ({ name, image }: Category) => {
  return (
    <Link
      href={"/"}
      className="relative isolate flex flex-col justify-end overflow-hidden rounded-2xl px-8 pb-8 pt-40 w-11/12 mx-auto mt-5"
    >
      <Image
        src={image}
        width={400}
        height={400}
        alt="University of Southern California"
        className="absolute inset-0 h-full w-full object-cover"
      />
      <div className="absolute inset-0 bg-gradient-to-t from-gray-900 via-gray-900/40"></div>
      <h3 className="z-10 mt-3 text-3xl font-bold text-white">{name}</h3>
    </Link>
  );
};

export default CategoryCard;
