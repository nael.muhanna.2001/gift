import CreditCardIcon from "@mui/icons-material/CreditCard";
const Point = () => {
  return (
    <>
      <div className="p-4 md:w-1/4 w-1/2">
        <div className="border-2 border-gray-600 px-4 py-6 rounded-lg transform transition duration-500 hover:scale-110">
          <CreditCardIcon
            className="text-orange-500 w-12 h-12 mb-3 inline-block"
            sx={{ fontSize: 40 }}
          />
          <h2 className="title-font font-medium text-3xl text-gray-900">50</h2>
          <p className="leading-relaxed">نقطة</p>
        </div>
      </div>
    </>
  );
};

export default Point;
