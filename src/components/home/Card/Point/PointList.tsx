import React from "react";
import Point from "./Point";
import Link from "next/link";
const PointList = () => {
  return (
    <Link href={"#"} className="text-gray-700 body-font">
      <div className="container px-5 py-24 mx-auto">
        <div className="flex flex-wrap -m-4 text-center">
          <Point />
          <Point />
          <Point />
          <Point />
          <Point />
        </div>
      </div>
    </Link>
  );
};

export default PointList;
