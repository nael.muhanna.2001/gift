"use client";

import { Email, Password } from "@mui/icons-material";
import axios from "axios";
import { useState, useEffect } from "react";
const LoginForm = () => {
  const [user, setUser] = useState({ email: "", password: "" });
  const loginUser = (e: React.FormEvent) => {
    e.preventDefault();
  };
  const print = async () => {
    const userInfo = await axios.post(`${process.env.URL}/auth/login`, user);
    console.log(userInfo.data);
  };
  return (
    <form className="space-y-6" onSubmit={loginUser}>
      <div>
        <label htmlFor="email" className="block font-medium text-gray-700">
          البريد الإلكتروني
        </label>
        <div className="mt-1">
          <input
            id="email"
            name="email"
            type="email"
            autoComplete="email"
            value={user.email}
            onChange={(e) => setUser({ ...user, email: e.target.value })}
            required
            className="block w-full appearance-none rounded-md border border-gray-300 px-3 py-2 placeholder-gray-400 shadow-sm focus:border-amber-500 focus:outline-none focus:ring-amber-500 sm:text-sm"
          />
        </div>
      </div>

      <div>
        <label htmlFor="password" className="block font-medium text-gray-700">
          كلمة المرور
        </label>
        <div className="mt-1">
          <input
            id="password"
            name="password"
            type="password"
            autoComplete="current-password"
            value={user.password}
            onChange={(e) => setUser({ ...user, password: e.target.value })}
            required
            className="block w-full appearance-none rounded-md border border-gray-300 px-3 py-2 placeholder-gray-400 shadow-sm focus:border-amber-500 focus:outline-none focus:ring-amber-500 sm:text-sm"
          />
        </div>
      </div>

      <div>
        <button
          type="submit"
          onClick={print}
          className="flex w-full justify-center rounded-md border border-transparent bg-amber-500 py-2 px-4 text-sm font-medium text-white shadow-sm hover:bg-amber-500 focus:outline-none focus:ring-2 focus:bg-amber-500 focus:ring-offset-2"
        >
          تسجيل الدخول
        </button>
      </div>
    </form>
  );
};

export default LoginForm;
